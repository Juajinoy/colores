﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace colores
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page1 : ContentPage
	{
		public Page1 ()
		{
			InitializeComponent ();
		}

        //INICIO NUMERO DE SLIDER
        void ValueChanged(object sender, ValueChangedEventArgs args)
        {

            
            if (sender == id_sl_high)
            {

                id_lb_valueHigh.Text = "" + (int)id_sl_high.Value;
                id_lb_valueWidth.Text = "" + (int)id_sl_high.Value;
            }

            else if (sender == id_sl_width)
            {
                id_lb_valueWidth.Text = "" + (int)id_sl_width.Value;
                id_lb_valueHigh.Text = "" + (int)id_sl_width.Value;
            }

            //id_bv_background.BackgroundColor = Color.FromRgba((int)id_sl_red.Value, (int)id_sl_green.Value, (int)id_sl_blue.Value, (int)id_sl_alpha.Value);

            if (sender==id_sl_high)
            {
                id_bv_page1.HeightRequest = (int)id_sl_high.Value;
                id_bv_page1.WidthRequest = (int)id_sl_high.Value;
                //id_sl_width.Value = (int)id_sl_high.Value;

            }else if (sender == id_sl_width)
            {
                id_bv_page1.HeightRequest = (int)id_sl_width.Value;
                id_bv_page1.WidthRequest = (int)id_sl_width.Value;
                //id_sl_high.Value = (int)id_sl_width.Value;
            }

           



        }
        //FIN NUMERO DE SLIDER
    }
}