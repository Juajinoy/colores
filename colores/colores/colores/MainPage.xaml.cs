﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace colores
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        //INICIO METODO DE PROGRAMACION DEL SLIDER
        void OnSliderValueChanged(object sender, ValueChangedEventArgs args) {

            /*if (sender == id_sl_blue)
            {
                DisplayAlert("Error","Prueba de colores","OK");
            }*/
            //Color.FromRgba(): se lo utiliza para colocar el color, recibe 4 parametros 
            // id_sl_red.Value lo utilizamos para obtener el valor del numero
            // le asigno la combinacion de colores en el boxView en backgroundcolor
            if (sender==id_sl_red)
            {
              
                labelMessage.Text = ""+ (int)id_sl_red.Value;
            }
            else if (sender == id_sl_green)
            {
                labelMessage_green.Text =""+(int)id_sl_green.Value;                
            }
            else if (sender == id_sl_blue)
            {
                labelMessage_blue.Text = "" + (int)id_sl_blue.Value;
            }
            else if (sender == id_sl_alpha)
            {
                labelMessage_alpha.Text = "" + (int)id_sl_alpha.Value;
            }
               id_bv_background.BackgroundColor=Color.FromRgba((int)id_sl_red.Value, (int)id_sl_green.Value, (int)id_sl_blue.Value, (int)id_sl_alpha.Value);
        }

        //FIN METODO DE PROGRAMACION DEL SLIDER

        //INICIO DE PROGRAMACION DE BOTON
        async private void ChangePage(object sender, EventArgs e)
        {
                await Navigation.PushAsync(new Page1());
               
        }
        //FIN DE PROGRAMACION DE BOTON
    }
}
